# Mutiple Choice Questions

*Output: `multi_choice.txt`,  ( 1 answer per line)*

*Example:*
```
> cat multi_choice.txt
A
B
C
```

******



####1. Which of the following is a *`adjacent sibling selector`* in CSS

A. +

B. ~

C. >

D. space

--

####2. Consider the below JavaScript code:

```js
function hack(){
    console.log(myvar);
}

function hacker(){
    var myvar=2;
    hack();

}
var myvar=1;
hacker();
```

What is the output of above code ?

A. undefined

B. 1

C. 2

D. undefined  1

--

####3. How do you insert padding so that it is:
```
10 pixels at the top
15 pixels at the bottom
5 pixels at the right
10 pixels to the left
```

A. padding:10px 15px 5px 10px;

B. padding:15px 5px 10px 10px;

C. padding:10px 5px 10px 15px;

D. padding:10px 5px 15px 10px;

--

####4. Which of the following is not event of `<video>` element?

A. play

B. pause

C. progress

D. error

E. All of the above

--

####5. Predict the output of below JavaScript code:
```js
const pi=3.14;
var pi=4;
console.log(pi);
```
A. Prints 4

B. Prints 3.14

C. It will give an error

D. None of the above

--

####6. What is box sizing?

A. Specifies whether or not an element should be re-sizable by the user

B. Allows you to define certain elements to fit an area in a certain way

C. Draws outline beyond the border edge

D. All the above

--

####7. Which of the following is not considered as an error in JavaScript?

A. Syntax error

B. Missing of semicolons

C. Division by zero

D. All the above

--

####8. Which of these contain meta data?

A. `<html>` tag

B. `<a>` tag

C. `<head>` tag

D. None of these

--

####9. What is the result of the below code?
```js
NaN === NaN;
```
A. true

B. false

C. TypeError

D. Uncaught exception

--

####10. What should be the expected output of the below code?
```html
<script type="text/javascript">
    y=4+"4";
    document.write(y);
</script>
```

A. 44

B. 8

C. 4

D. 4"4"

--

# Programming Questions


*******

####11. Sum Game
A positive integer may be expressed as a sum of different numbers, in one way or another. Consider N=a+b+c+d where a,b,c,d are positive integers greater than 0 and a<=b<=c<=d.

You have to count the number of ways to express N as sum of 4 such numbers.

**INPUT FORMAT:**
The first line will contain t - number of test cases
Each of the next t lines contain a single integer, N.

**OUTPUT FORMAT:**
For each test case, print the number of possible combinations of a, b, c and d such that
a+b+c+d=N, a<=b<=c<=d and a,b,c,d>0.

**Constraints:**
1 <= t <= 10
1 <= N <= 5000

Your task is to implement a JS application which reads data from an input file and writes the results to an output file. Test cases can be found in `/data`

**Note: Use Javascript (preferably ES6) to solve this problem**

Sample Input:
```
2
5
6
```

Sample Output:
```
1
2
```

Sample Explanation:
```
For N = 5, there is only 1 way:
1+1+1+2
For N = 6, there are 2 ways:
1+1+1+3
1+1+2+2
```

--

####12. Autoscroll Chat Component
Implement a reusable autoscroll React Component for a message box of a chat app that keep the message box scrolled to the bottom whenever a new message comes, but not if the user has scrolled up or out of the element.

Sample input data : [http://codepaste.net/iaoorx][1]

You can streamline the transcript to simulate an ongoing conversation
( 1 message per line) .

*** Note: You can use any similar input data, and it can be hard-coded in the source code***

Example usage:

```html
<AutoScroll>
  <ul className="messages" style={{
    height: '300px',
    border: '1px solid',
    overflow: 'auto'
  }}>
  {lines.map((line, index) => (
    <li key={index}>{line}</li>
  ))}
  </ul>
</AutoScroll >
```
**Tips: Use a simple React Starter Kit can save you some time !**


  [1]: http://codepaste.net/iaoorx


